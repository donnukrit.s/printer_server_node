
const escpos = require('escpos')
const USB = require('escpos-usb');

const findPrinter = () => {
    let printers = USB.findPrinter()
    if (printers.length > 0) {
        console.log(`Found ${printers.length} Printers`)
        return { "status": true, "count": printers.length, "msg": "printer found" }
    } else {
        console.log(`Printer Not Found`)
        return { "status": false, "msg": "printer not found" }
    }
}

const printText = async () => {
    try {
        const device = USB.findPrinter()
        console.log(device)
        await device[0].open(function (err) {
            printer
                .model('qsprinter')
                .font('a')
                .align('ct')
                .style('bu')
                .size(1, 1)
                .encode('tis620')
                .text('The quick brown fox jumps over the lazy dog')
                .text('สวัสดีภาษาไทย')
                .close();

        });
        return { "status": true, "msg": "printer accept command" }
    } catch {
        return { "status": true, "msg": "printer not accept command" }
    }


}

module.exports = { findPrinter, printText }


