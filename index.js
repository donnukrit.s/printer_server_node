

const { findPrinter } = require('escpos-usb')
const express = require('express')


const printer_module = require('./modules/printer-module')
const app = express()

app.use(express.json())

app.get("/findPrinter", (req, res) => {
    let data = printer_module.findPrinter()
    return res.json(data)

})

app.get("/printText", async (req, res) => {
    let data = await printer_module.printText()
    console.log(data)
    return res.json(data)
})
// route

app.listen(8080, () => { console.log("server is running") })